package aed;

import java.util.*;

class Funciones {
    int cuadrado(int x) {
        return x * x;
    }

    double distancia(double x, double y) {
        double res = Math.sqrt(x * x + y * y);
        return res;
    }

    boolean esPar(int n) {
        boolean res = false;
        if (n % 2 == 0) {
            res = true;
        }
        return res;
    }

    boolean esBisiesto(int n) {
        if ((n % 4 == 0 && n % 100 != 0) || n % 400 == 0) {
            return true;
        }
        return false;
    }

    int factorialIterativo(int n) {
        int res = 1;
        if (n == 0) {
            res = 1;
        } else {
            for (int i = 1; i <= n; i++) {
                res *= i;
            }
        }
        return res;
    }

    int factorialRecursivo(int n) {
        int res = 0;
        if (n == 0) {
            res = 1;
        } else {
            res = n * factorialRecursivo(n - 1);
        }
        return res;
    }

    boolean esPrimo(int n) {
        boolean res = true;
        if (n == 0 || n == 1) {
            res = false;
        }
        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                res = false;
            }
        }
        return res;
    }

    int sumatoria(int[] numeros) {
        int res = 0;
        for (int num : numeros) {
            res += num;
        }
        return res;
    }

    int busqueda(int[] numeros, int buscado) {
        int res = -1;
        for (int i = 0; i < numeros.length; i++) {
            if (numeros[i] == buscado) {
                res = i;
            }
        }
        return res;
    }

    boolean tienePrimo(int[] numeros) {
        boolean res = false;
        for (int i = 0; i < numeros.length; i++) {
            if (esPrimo(numeros[i])) {
                res = true;
            }
        }
        return res;
    }

    boolean todosPares(int[] numeros) {
        for (int i = 0; i < numeros.length; i++) {
            if (!esPar(numeros[i])) {
                return false;
            }
        }
        return true;
    }

    boolean esPrefijo(String s1, String s2) {
        if (s1.length() > s2.length()) {
            return false;
        }
        for (int i = 0; i < s1.length(); i++) {
            if (s1.charAt(i) != s2.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    boolean esSufijo(String s1, String s2) {
        if (s1.length() > s2.length()) {
            return false;
        }
        int j = 0;
        for (int i = s2.length() - s1.length(); i < s2.length(); i++) {

            if (s1.charAt(j) != s2.charAt(i)) {
                return false;
            }
            if (j < s1.length()) {
                j++;
            }
        }
        return true;
    }
}
