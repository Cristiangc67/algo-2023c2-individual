package aed;

import java.util.Scanner;
import java.io.PrintStream;

class Archivos {
    float[] leerVector(Scanner entrada, int largo) {
        float[] vec = new float[largo];
        for (int i = 0; i < largo; i++) {
            vec[i] = entrada.nextFloat();
        }

        return vec;
    }

    float[][] leerMatriz(Scanner entrada, int filas, int columnas) {
        float[][] vec = new float[filas][columnas];

        for (int i = 0; i < filas; i++) {
            vec[i] = leerVector(entrada, columnas);
        }

        return vec;
    }

    void imprimirPiramide(PrintStream salida, int alto) {
        if (alto <= 0) {

        } else {
            for (int i = 0; i < alto; i++) {
                for (int j = alto - 1 - i * 1; j > 0; j--) {
                    salida.print(" ");
                }
                for (int j = 0; j < 2 * i + 1; j++) {
                    salida.print("*");
                }
                for (int j = alto - 1 - i * 1; j > 0; j--) {
                    salida.print(" ");
                }
                salida.println();

            }
        }
    }
}
