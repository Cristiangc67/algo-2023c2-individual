package aed;

import java.util.*;

// Todos los tipos de datos "Comparables" tienen el método compareTo()
// elem1.compareTo(elem2) devuelve un entero. Si es mayor a 0, entonces elem1 > elem2
public class ABB<T extends Comparable<T>> implements Conjunto<T> {
    // Agregar atributos privados del Conjunto
    private int tamaño;
    private Nodo origen;

    private class Nodo {
        // Agregar atributos privados del Nodo
        private Nodo padre;
        private T valor;
        private Nodo izq;
        private Nodo der;

        // Crear Constructor del nodo
        private Nodo(T valor) {
            this.padre = null;
            this.valor = valor;
            this.izq = null;
            this.der = null;
        }

    }

    public ABB() {
        tamaño = 0;
        origen = null;
    }

    public int cardinal() {
        return tamaño;
    }

    public int cardinalAux(Nodo origen) {
        if (origen == null) {
            return 0;
        }
        int izquierda = cardinalAux(origen.izq);
        int derecha = cardinalAux(origen.der);

        return 1 + izquierda + derecha;
    }

    public T minimo() {
        if (origen == null) {
            return null;
        }
        Nodo actual = origen;

        while (actual.izq != null) {
            actual = actual.izq;
        }
        return actual.valor;
    }

    public T maximo() {
        if (origen == null) {
            return null;
        }
        Nodo actual = origen;
        while (actual.der != null) {
            actual = actual.der;
        }
        return actual.valor;
    }

    public void insertar(T elem) {
        origen = insertarAux(origen, elem);
    }

    public Nodo insertarAux(Nodo origen, T valor) {
        if (origen == null) {
            tamaño++;
            return new Nodo(valor);
        }

        int comparacion = valor.compareTo(origen.valor);
        if (comparacion < 0) {
            origen.izq = insertarAux(origen.izq, valor);
        } else if (comparacion > 0) {
            origen.der = insertarAux(origen.der, valor);
        }

        return origen;
    }

    public boolean pertenece(T elem) {
        return perteneceAux(origen, elem) != null;
    }

    private Nodo perteneceAux(Nodo origen, T elem) {
        if (origen == null) {
            return null;
        }

        int comparacion = elem.compareTo(origen.valor);
        if (comparacion == 0) {
            return origen;
        } else if (comparacion < 0) {
            return perteneceAux(origen.izq, elem);
        } else {
            return perteneceAux(origen.der, elem);
        }
    }

    public void eliminar(T elem) {
        origen = eliminarAux(origen, elem);
        tamaño--;
    }

    private Nodo eliminarAux(Nodo origen, T elem) {

        if (origen == null) {
            return origen;
        }

        int comparacion = elem.compareTo(origen.valor);
        if (comparacion < 0) {
            origen.izq = eliminarAux(origen.izq, elem);
        } else if (comparacion > 0) {
            origen.der = eliminarAux(origen.der, elem);
        } else {
            if (origen.izq == null) {
                return origen.der;
            } else if (origen.der == null) {
                return origen.izq;
            }
            origen.valor = minimoParam(origen.der);
            origen.der = eliminarAux(origen.der, origen.valor);
        }

        return origen;
    }

    private T minimoParam(Nodo origen) {
        T minimo = origen.valor;
        while (origen.izq != null) {
            minimo = origen.izq.valor;
            origen = origen.izq;
        }
        return minimo;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        imprimirEnOrden(origen, sb);
        sb.delete(sb.length() - 1, sb.length());
        sb.append("}");
        return sb.toString();
    }

    public void imprimirEnOrden(Nodo origen, StringBuilder sb) {
        if (origen != null) {
            imprimirEnOrden(origen.izq, sb);
            sb.append(origen.valor);

            sb.append(",");

            imprimirEnOrden(origen.der, sb);
        }

    }

    public Nodo minimoNodo() {
        if (origen == null) {
            return null;
        }
        Nodo actual = origen;

        while (actual.izq != null) {
            actual = actual.izq;
        }
        return actual;
    }

    private class ABB_Iterador implements Iterador<T> {
        private Nodo _actual = null;

        public boolean haySiguiente() {
            return minimoNodo() != null;
        }

        public T siguiente() {
            if (_actual != null) {
                eliminar(_actual.valor);
            }
            _actual = minimoNodo();
            return _actual.valor;
        }
    }

    public Iterador<T> iterador() {
        return new ABB_Iterador();
    }

}
