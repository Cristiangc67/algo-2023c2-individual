package aed;

import java.util.*;

public class ListaEnlazada<T> implements Secuencia<T> {
    // Completar atributos privados
    private int longitud = 0;
    private Nodo<T> primerElem;
    private Nodo<T> ultimoElem;

    private class Nodo<T> {
        // Completar
        T elemento;
        Nodo<T> siguiente;
        Nodo<T> anterior;

        Nodo(T elemento) {
            this.elemento = elemento;
            this.siguiente = null;
            this.anterior = null;
        }

    }

    public ListaEnlazada() {
        longitud = 0;
        primerElem = null;
        ultimoElem = null;
    }

    public int longitud() {
        return longitud;
    }

    public void agregarAdelante(T elem) {
        Nodo<T> nuevoNodo = new Nodo<>(elem);
        if (primerElem == null) {
            primerElem = nuevoNodo;
            ultimoElem = nuevoNodo;
        } else {
            nuevoNodo.siguiente = primerElem;
            primerElem.anterior = nuevoNodo;
            primerElem = nuevoNodo;
        }
        longitud++;
    }

    public void agregarAtras(T elem) {
        Nodo<T> nuevoNodo = new Nodo<>(elem);
        if (primerElem == null) {
            primerElem = nuevoNodo;
            ultimoElem = nuevoNodo;
        } else {
            nuevoNodo.anterior = ultimoElem;
            ultimoElem.siguiente = nuevoNodo;
            ultimoElem = nuevoNodo;
        }
        longitud++;
    }

    public T obtener(int i) {
        if (i < 0 || i >= longitud) {
            System.out.println("indice fuera de rango");
        }
        Nodo<T> actual = primerElem;
        int j = 0;
        while (j < i) {
            actual = actual.siguiente;
            j++;
        }

        return actual.elemento;
    }

    public void eliminar(int i) {
        if (i < 0 || i >= longitud) {
            System.out.println("indice fuera de rango");
        }
        Nodo<T> nodoAEliminar = primerElem;
        if (i == 0) {
            primerElem = primerElem.siguiente;
            if (primerElem != null) {
                primerElem.anterior = null;
            } else {
                ultimoElem = null;
            }
        } else if (i == longitud - 1) {
            nodoAEliminar = ultimoElem;
            ultimoElem = ultimoElem.anterior;
            ultimoElem.siguiente = null;

        } else {
            int j = 0;
            while (j < i) {
                nodoAEliminar = nodoAEliminar.siguiente;
                j++;
            }
            nodoAEliminar.anterior.siguiente = nodoAEliminar.siguiente;
            nodoAEliminar.siguiente.anterior = nodoAEliminar.anterior;
        }
        longitud--;

    }

    public void modificarPosicion(int indice, T elem) {
        if (indice < 0 || indice >= longitud) {
            System.out.println("indice fuera de rango");
        }
        Nodo<T> nodoACambiar = primerElem;
        if (indice == 0 && primerElem != null) {
            primerElem.elemento = elem;
        } else if (longitud - 1 == indice && primerElem != null) {
            ultimoElem.elemento = elem;
        } else {
            int contador = 0;
            while (indice != contador) {
                nodoACambiar = nodoACambiar.siguiente;
                contador++;
            }
            nodoACambiar.elemento = elem;
        }

    }

    public ListaEnlazada<T> copiar() {
        ListaEnlazada<T> copia = new ListaEnlazada<>();
        Nodo<T> actual = primerElem;
        while (actual != null) {
            copia.agregarAtras(actual.elemento);
            actual = actual.siguiente;
        }
        return copia;

    }

    public ListaEnlazada(ListaEnlazada<T> lista) {

        ListaEnlazada<T> copia = new ListaEnlazada<>();
        Nodo<T> actual = lista.primerElem;
        while (actual != null) {
            Nodo<T> nuevoNodoCopia = new Nodo<>(actual.elemento);
            copia.agregarAtras(nuevoNodoCopia.elemento);
            actual = actual.siguiente;
        }

        this.longitud = copia.longitud;
        this.primerElem = copia.primerElem;
        this.ultimoElem = copia.ultimoElem;

    }

    @Override
    public String toString() {
        StringBuilder secuencia = new StringBuilder();
        secuencia.append("[");
        Nodo<T> actual = primerElem;

        while (actual != null) {
            secuencia.append(actual.elemento);
            actual = actual.siguiente;
            if (actual != null) {
                secuencia.append(", ");
            }
        }
        secuencia.append("]");
        return secuencia.toString();
    }

    private class ListaIterador implements Iterador<T> {
        // Completar atributos privados
        private Nodo<T> actual = primerElem;
        private Nodo<T> anterior = null;

        public boolean haySiguiente() {
            /*
             * Nodo<T> copySig = actual;
             * if (copySig != null) {
             * return true;
             * } else {
             * return false;
             * }
             */
            return actual != null;

        }

        public boolean hayAnterior() {
            /*
             * Nodo<T> copyAnt;
             * if (actual != null) {
             * copyAnt = actual.anterior;
             * } else {
             * copyAnt = null;
             * if (copyAnt != null) {
             * return true;
             * } else {
             * return false;
             * }
             * }
             */
            return anterior != null;
        }

        public T siguiente() {

            if (this.haySiguiente()) {
                T elemento = actual.elemento;
                actual = actual.siguiente;
                if (anterior != null) {
                    anterior = anterior.siguiente;
                } else {
                    anterior = primerElem;
                }
                return elemento;
            } else {
                return null;
            }
        }

        public T anterior() {

            if (this.hayAnterior()) {
                T elemento = anterior.elemento;
                if (actual != null) {
                    anterior = anterior.anterior;
                    actual = actual.anterior;
                } else {
                    anterior = anterior.anterior;
                    actual = ultimoElem;
                }
                return elemento;
            } else {
                return null;
            }

        }
    }

    public Iterador<T> iterador() {
        return new ListaIterador();
    }

}
